// In App.js in a new project

import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SettingsPage from './../screens/settings/SettingsPage';
import WifiPage from './../screens/settings/WifiPage';
import irrigation from '../screens/settings/irrigation';

import {Icon} from 'react-native-gradient-icon';

const Stack = createNativeStackNavigator();

function Settings() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SETTINGS"
        component={SettingsPage}
        options={{
          headerTitleAlign: 'center',
          headerRight: () => (
            <Icon
              name="more-vertical"
              size={30}
              colors={[
                {color: '#2BD29A', offset: '0', opacity: '1'},
                {color: '#A8E13A', offset: '1', opacity: '1'},
              ]}
            />
          ),
        }}
      />
      <Stack.Screen
        name="WifiPage"
        component={WifiPage}
        options={{title: 'WIFI SETTINGS', headerTitleAlign:'center'}}
      />
      <Stack.Screen name="irrigation" component={irrigation} />
    </Stack.Navigator>
  );
}

export default Settings;
