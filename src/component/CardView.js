import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {Card, Paragraph, Title} from 'react-native-paper';
import {Icon} from 'react-native-gradient-icon';

import {height, width} from 'react-native-dimension';

const CardView = props => {
  return (
    <TouchableOpacity onPress={props.navigation} style={styles.container}>
      <View style={{...styles.card, ...props.style}}>
        <View style={styles.makeCenter}>
          <Image style={styles.imageContainer} source={props.image} />
        </View>
        <View style={{...styles.detailsContainer, ...props.customStyle}}>
          <Card.Content>
            <Title style={styles.textTitle}>{props.title}</Title>
            <Paragraph style={styles.textDescription}>
              {props.description}
            </Paragraph>
          </Card.Content>
        </View>
        <View style={styles.makeCenter}>
          <TouchableOpacity onPress={props.navigation}>
            <Icon
              name="chevron-right"
              size={30}
              colors={[
                {color: '#2DAC59', offset: '0', opacity: '1'},
                {color: '#A8E13A', offset: '1', opacity: '1'},
              ]}
            />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  card: {
    // marginTop: 20,
    marginVertical: height(1.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width(90),
    height: height(10),
    backgroundColor: 'white',
    borderRadius: 15,
    // elevation: 10,
  },
  imageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 55,
    height: 55,
    marginLeft: 10,
  },
  detailsContainer: {
    width: width(65),
    height: height(10),
  },
  textTitle: {
    fontSize: 13,
  },
  textDescription: {
    fontSize: height(1.2),
    textAlign: 'justify',
    lineHeight: 13,
  },
  iconStyle: {
    marginLeft: -20,
    marginTop: 10,
  },
  makeCenter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default CardView;
