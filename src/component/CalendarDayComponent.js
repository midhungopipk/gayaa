import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import PropTypes from 'prop-types';

const weekDaysNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

const CalendarDayComponent = props => {
  const highDemandImage = require('../IconImages/dry.png');
  const onDayPress = () => {
    props.onPress();
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        {props.horizontal ? (
          <Text style={styles.weekName} numberOfLines={1}>
            {weekDaysNames[props.date.weekDay].toUpperCase()}
          </Text>
        ) : null}
      </View>
      <TouchableOpacity
        style={[styles.content]}
        onPress={() => {
          onDayPress();
        }}>
        {props.m ? (
          <View style={styles.icon}>
            <Text style={[styles.contentText]}>
              {String(props.children.day)}
            </Text>
            <Image source={highDemandImage} style={styles.smallIcon} />
          </View>
        ) : (
          <Text style={[styles.contentText]}>{String(props.children.day)}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

CalendarDayComponent.propTypes = {
  children: PropTypes.any,
  state: PropTypes.string,
  marking: PropTypes.any,
  horizontal: PropTypes.bool,
  date: PropTypes.object,
  onPress: PropTypes.func.isRequired,
  current: PropTypes.string,
  copilot: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 7,
    marginRight: 7,
  },
  weekName: {
    width: 32,
    textAlign: 'center',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#7c7c7c',
  },
  content: {
    width: 39,
    height: 50,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#ddd',
    marginBottom: 20,
  },
  contentText: {
    fontSize: 20,
  },
  footer: {
    flexDirection: 'row',
  },
  smallIcon: {
    width: 12,
    height: 12,
    //position: 'absolute',
    top: -1,
    right: -1,
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default CalendarDayComponent;
