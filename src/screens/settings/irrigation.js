import * as React from 'react';
import {View, StyleSheet} from 'react-native';
import {Calendar} from 'react-native-calendars';
import CalendarDayComponent from '../../component/CalendarDayComponent';

const Irrigation = () => {
  const [markedDate, setMarkedDate] = React.useState(['2022-02-17']);

  const onDayPress = async day => {
    if (markedDate.length === 0) {
      markedDate.push(day.dateString);
    } else {
      setMarkedDate(oldArray => [...oldArray, day.dateString]);
    }
  };

  return (
    <Calendar
      //onDayPress={this.onDayPress}
      dayComponent={({date, state, marking}) => {
        return (
          <CalendarDayComponent
            children={date}
            onPress={() => {
              onDayPress(date);
            }}
            m={markedDate.includes(date.dateString)}
          />
        );
      }}
      markingType={'custom'}
      minDate={Date()}
      current={'2022-02-10'}
      monthFormat={'MMMM yyyy'}
      hideExtraDays={true}
      hideDayNames={false}
      enableSwipeMonths={true}
      style={{
        borderWidth: 1,
        borderColor: 'gray',
        height: 350,
      }}
      // Specify theme properties to override specific styles for calendar parts. Default = {}
      // theme={{
      //   backgroundColor: '#ddd',
      //   calendarBackground: '#ddd',
      // }}
    />
  );
};

export default Irrigation;
