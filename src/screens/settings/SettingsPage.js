import {View, StyleSheet, Text} from 'react-native';
import React from 'react';

import CardView from '../../component/CardView';
import {height, width} from 'react-native-dimension';

const SettingsPage = ({navigation}) => {
  return (
    <View style={styles.container}>
      <CardView
        customStyle={{height: height(12), paddingTop: 6}}
        style={styles.card}
        image={require('./../../IconImages/irrigation.png')}
        title="IRRIGATION"
        description="The irrigation is always ON in the greenhouse , it is only when you
        choose to have designate dry days or you shut it off manually
        through the app that irrigation turn off."
        navigation={() => navigation.navigate('irrigation')}
      />
      <CardView
        customStyle={{height: height(12), paddingTop: 6}}
        style={styles.card}
        image={require('./../../IconImages/sun.png')}
        title="LIGHT CYCLE"
        description="The light cycle will be 16 hours of light and 8 hours of darkness by default setting."
      />
      <CardView
        customStyle={{height: height(12), paddingTop: 6}}
        style={styles.card}
        image={require('./../../IconImages/ventilation.png')}
        title="VENTILATION"
        description="Chooses to start or stop the fan by presenting a button.If the fan is on a timer it bypasses the timer state."
      />

      <View style={styles.textContainer}>
        <Text style={styles.textWifi}>WiFi and Bluetooth Settings</Text>
        <Text style={styles.textSub}>Make pairing</Text>
      </View>

      <CardView
        customStyle={{justifyContent: 'center'}}
        image={require('./../../IconImages/wifi.png')}
        title="WiFi"
        description="Not connected"
        navigation={() => navigation.navigate('WifiPage')}
      />
      <CardView
        customStyle={{justifyContent: 'center'}}
        image={require('./../../IconImages/Vector.jpg')}
        title="Bluetooth"
        description="ON"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    height: height(14),
  },
  textContainer: {
    marginRight: width(30),
    marginTop: 20,
  },
  textWifi: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  textSub: {
    paddingTop: 10,
  },
});

export default SettingsPage;
