import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Switch,
  FlatList,
  TouchableNativeFeedback,
  PermissionsAndroid,
  Modal,
  ScrollView,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {Button, Card, IconButton, TextInput} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import wifi from 'react-native-android-wifi';
import SystemSetting from 'react-native-system-setting';
import {useFocusEffect} from '@react-navigation/native';
import {height, width, totalSize} from 'react-native-dimension';

const WifiPage = props => {
  const [switchValue, setSwitchValue] = useState(false);
  const [wifiList, setWifiList] = useState([]);
  const [isNetworkAvailable, setIsNetworkAvailable] = useState();
  const [isVisible, setIsvisible] = useState(false);
  const [ssid, setssid] = useState('');
  const [password, setPassword] = useState('');
  const [wifiCredentials, setWifiCredentials] = useState({});
  const [currentSSID, setCurrentSSID] = useState('');
  const [permission, setPermission] = useState(false);
  const [askToJoin, setAskToJoin] = useState('Off');
  const [autoJoin, setAutoJoin] = useState('Automatic');

  // console.log(wifiCredentials);

  const verifyPermission = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Wifi networks',
        message: 'We need your permission in order to find wifi networks',
        buttonPositive: 'Allow',
        buttonNegative: 'Deny',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      setPermission(true);
    } else {
      setPermission(false);
    }
  };

  useEffect(() => {
    verifyPermission();
    if (!permission) {
      verifyPermission();
    }
  }, [permission]);

  useFocusEffect(() => {
    wifi.isEnabled(isEnabled => {
      if (isEnabled) {
        setIsNetworkAvailable(isEnabled);
        setSwitchValue(true);

        wifi.getSSID(ssid => {
          setCurrentSSID(ssid);
        });
        wifi.loadWifiList(
          wifiStringList => {
            var wifiArray = JSON.parse(wifiStringList);
            setWifiList(wifiArray);
          },
          error => {
            console.log(error);
          },
        );
      } else {
        setSwitchValue(false);
        setIsNetworkAvailable(false);
        return;
      }
    });
  });

  const toggleSwitch = value => {
    SystemSetting.switchWifi(() => {
      setSwitchValue(value);
    });
  };

  const wifissidHandler = ssid => {
    {
      Platform.OS === 'android' && setIsvisible(true);
    }
    setssid(ssid);
  };

  const PasswordHandler = password => {
    setPassword(password);
  };

  const saveWifiCredentials = () => {
    setIsvisible(false);
    const updated = {SSID: ssid, password: password};
    setWifiCredentials(updated);
    setPassword('');
    setssid('');
  };

  const availableNetworkList = itemData => {
    return (
      <View style={styles.networkOptionCards}>
        <TouchableNativeFeedback
          onPress={wifissidHandler.bind(this, itemData.item.SSID)}>
          <Card
            style={{
              ...styles.NetworksCard,
              borderColor:
                currentSSID === itemData.item.SSID ? 'black' : 'white',
              borderWidth: 3,
            }}>
            <View style={styles.cardContent}>
              <Text style={styles.cardText}>{itemData.item.SSID}</Text>
              <MaterialCommunityIcons name="wifi" color="black" size={25} />
            </View>
          </Card>
        </TouchableNativeFeedback>
      </View>
    );
  };

  return (
    <View style={{flex: 1, marginBottom: height(4)}}>
      <View style={styles.screen}>
        <Card style={styles.card}>
          <View style={styles.cardContent}>
            <Text style={styles.cardText}>WiFi</Text>
            <Switch
              trackColor={{false: '#767577', true: '#A8E13A'}}
              thumbColor="white"
              value={switchValue}
              onValueChange={toggleSwitch}
              style={{transform: [{scaleX: 1.5}, {scaleY: 1.5}]}}></Switch>
          </View>
        </Card>
      </View>
      <Text style={styles.network}>Network</Text>
      {isNetworkAvailable && permission ? (
        Platform.OS === 'android' ? (
          <View style={{alignItems: 'center'}}>
            <FlatList
              data={wifiList}
              keyExtractor={(value, index) => index.toString()}
              renderItem={availableNetworkList}
              style={styles.wifiList}
              showsVerticalScrollIndicator={false}
            />
          </View>
        ) : (
          <View style={styles.iosConnectionField}>
            <Text
              style={{
                fontSize: totalSize(2),
                color: 'black',
                fontWeight: 'bold',
                margin: height(1),
              }}>
              SSID
            </Text>
            <TextInput
              style={{
                height: height(6),
                marginBottom: height(1),
                marginHorizontal: width(2),
              }}
              placeholder="Enter your network name"
              onChangeText={wifissidHandler}
              value={ssid}
              activeUnderlineColor="black"
            />
            <Text
              style={{
                fontSize: totalSize(2),
                color: 'black',
                fontWeight: 'bold',
                margin: height(1),
              }}>
              Password
            </Text>
            <TextInput
              style={{
                height: height(6),
                marginBottom: height(1),
                marginHorizontal: width(2),
              }}
              secureTextEntry
              placeholder="Password"
              onChangeText={PasswordHandler}
              value={password}
              activeUnderlineColor="black"
            />
            <View style={{alignItems: 'center'}}>
              <Button
                color="black"
                onPress={saveWifiCredentials}
                mode="contained"
                style={{width: width(40), margin: height(1), borderRadius: 25}}
                labelStyle={{fontSize: 15}}>
                Connect
              </Button>
            </View>
          </View>
        )
      ) : (
        <View style={{alignItems: 'center'}}>
          <Text>Check your WiFi settings or Please allow permission.</Text>
        </View>
      )}

      <View style={styles.networkOptionCards}>
        <Card style={styles.card}>
          <View style={styles.cardContent}>
            <Text style={styles.cardText}>Ask to Join Networks</Text>
            <View style={styles.networkOptionIcon}>
              <Text style={{padding: height(1), color: 'black'}}>
                {askToJoin}
              </Text>
              <IconButton
                icon="chevron-right"
                color="#A8E13A"
                size={25}
                onPress={() => {
                  {
                    askToJoin === 'Off'
                      ? setAskToJoin('On')
                      : setAskToJoin('Off');
                  }
                }}
              />
            </View>
          </View>
        </Card>
        <Text
          style={{
            marginHorizontal: width(8),
            fontSize: totalSize(1.3),
            color: 'grey',
          }}>
          Known networks will be joined automatically. If no known networks are
          available you will have to manually select a network.
        </Text>
      </View>
      <View style={styles.networkOptionCards}>
        <Card style={styles.card}>
          <View style={styles.cardContent}>
            <Text style={styles.cardText}>Auto Join Hotspots</Text>
            <View style={styles.networkOptionIcon}>
              <Text style={{padding: height(1), color: 'black'}}>
                {autoJoin}
              </Text>
              <IconButton
                icon="chevron-right"
                color="#A8E13A"
                size={25}
                onPress={() => {
                  {
                    autoJoin === 'Automatic'
                      ? setAutoJoin('Off')
                      : setAutoJoin('Automatic');
                  }
                }}
              />
            </View>
          </View>
        </Card>
        <Text
          style={{
            marginHorizontal: width(8),
            fontSize: totalSize(1.3),
            color: 'grey',
          }}>
          Allow this device to automatically discover nearby personal hotspots
          when no Wi-Fi networks is available.
        </Text>
      </View>
      <Modal
        visible={isVisible}
        animationType={'slide'}
        transparent={true}
        onRequestClose={() => setIsvisible(false)}>
        <View style={styles.modalBackground}>
          <View style={styles.modal}>
            <Text
              style={{
                fontSize: totalSize(2),
                color: 'black',
                fontWeight: 'bold',
                margin: height(2),
              }}>
              {ssid}
            </Text>
            <TextInput
              style={{
                height: height(6),
                marginBottom: height(3),
                marginHorizontal: width(2),
              }}
              secureTextEntry
              placeholder="Password"
              onChangeText={PasswordHandler}
              value={password}
              activeUnderlineColor="black"
            />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Button
                color="black"
                onPress={saveWifiCredentials}
                mode="contained"
                style={{width: width(40), margin: height(1), borderRadius: 25}}
                labelStyle={{fontSize: 15}}>
                Connect
              </Button>
              <Button
                color="black"
                onPress={() => setIsvisible(false)}
                mode="contained"
                style={{width: width(30), margin: height(1), borderRadius: 25}}
                labelStyle={{fontSize: 16}}>
                Cancel
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
  },
  card: {
    width: width(90),
    height: height(8),
    borderRadius: 15,
    marginVertical: height(4),
    elevation: 6,
    backgroundColor: 'white',
  },
  cardContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: width(2),
    marginLeft: width(3),
    width: '90%',
    alignItems: 'center',
  },
  cardText: {
    color: 'black',
    fontSize: totalSize(1.6),
  },
  network: {
    color: 'black',
    fontSize: totalSize(2),
    fontWeight: 'bold',
    marginLeft: width(5),
  },
  networkOptionCards: {
    alignItems: 'center',
  },
  networkOptionIcon: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  NetworksCard: {
    width: width(80),
    height: height(8),
    borderRadius: 15,
    marginVertical: width(2),
    elevation: 7,
    backgroundColor: 'white',
  },
  wifiList: {
    maxHeight: height(30),
    minHeight: height(20),
    width: width(90),
    borderBottomColor: '#ccc',
    borderBottomWidth: 2,
  },
  modal: {
    justifyContent: 'center',
    height: height(30),
    width: width(90),
    borderRadius: 10,
    marginTop: height(10),
    marginHorizontal: width(5),
    backgroundColor: 'white',
    elevation: 40,
  },
  iosConnectionField: {
    justifyContent: 'center',
    borderRadius: 10,
    marginTop: height(1),
    marginHorizontal: width(5),
    backgroundColor: 'white',
    elevation: 9,
    height: height(30),
    width: width(90),
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
});

export default WifiPage;
